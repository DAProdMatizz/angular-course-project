import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-auth-alert',
  templateUrl: './auth-alert.component.html',
  styleUrls: ['./auth-alert.component.css']
})
export class AuthAlertComponent implements OnInit {

  @Input() errorMessage: string;
  @Output() close = new EventEmitter<void>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onClose(): void {

    this.close.emit();
  }
}
