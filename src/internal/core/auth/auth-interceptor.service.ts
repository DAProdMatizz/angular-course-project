import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../../shared/service/authenticate/auth.service';
import {exhaustMap, take} from 'rxjs/operators';
import {LoggedUserDto} from '../../shared/model/auth/logged-user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService) {

  }

  private static addAuthHeaderToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {

    return request.clone({
      headers: new HttpHeaders(
        {['Authorization']: `Bearer ${token}`}
      )
    });
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return this.authService.loggedUser.pipe(
      take(1),
      exhaustMap((user: LoggedUserDto) => {
        if (!user) {
          return next.handle(request);
        }

        const modifiedRequest = AuthInterceptorService.addAuthHeaderToRequest(request, user.getToken());
        return next.handle(modifiedRequest);
      })
    );
  }
}
