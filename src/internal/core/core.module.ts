import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoadingSpinnerComponent} from './loading-spinner/loading-spinner.component';
import {AuthAlertComponent} from './auth/auth-alert/auth-alert.component';
import {PlaceholderDirective} from './placeholder/placeholder.directive';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptorService} from './auth/auth-interceptor.service';
import {ErrorInterceptorService} from './error/error-interceptor.service';

@NgModule({
  declarations: [
    LoadingSpinnerComponent,
    AuthAlertComponent,
    PlaceholderDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CommonModule,
    LoadingSpinnerComponent,
    AuthAlertComponent,
    PlaceholderDirective
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptorService, multi: true}
  ]
})
export class CoreModule {
}
