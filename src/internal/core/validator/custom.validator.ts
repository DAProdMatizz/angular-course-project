import {FormControl} from '@angular/forms';

export class CustomValidator {

  private static readonly INVALID_NUMBER_TEXT = 'invalidNumber';

  static invalidNumber(control: FormControl): { [s: string]: boolean } {

    const controlNumber = Number(control.value);
    if (controlNumber <= 0) {
      return {[CustomValidator.INVALID_NUMBER_TEXT]: true};
    }
    return null;
  }
}
