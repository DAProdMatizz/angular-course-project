import {Injectable} from '@angular/core';
import {IngredientDto} from '../../model/ingredient/ingredient.model';
import {Observable, Subject} from 'rxjs';
import {ShoppingListControllerService} from '../../controller/shopping-list/shopping-list-controller.service';

@Injectable({providedIn: 'root'})
export class ShoppingListService {

  ingredientsChanged = new Subject<IngredientDto[]>();
  startedEditing = new Subject<number>();

  constructor(private shoppingListController: ShoppingListControllerService) {

  }

  loadIngredients(): Observable<IngredientDto[]> {

    return this.shoppingListController.loadShoppingListIngredients();
  }

  getIngredientById(id: number): Observable<IngredientDto> {

    return this.shoppingListController.loadIngredientById(id);
  }

  addIngredient(newIngredient: IngredientDto): void {

    this.addIngredientAsPromise(newIngredient)
      .then(() => {
        this.loadIngredients().toPromise().then((ingredients: IngredientDto[]) => {
          this.ingredientsChanged.next(ingredients);
        });
      });
  }

  addIngredients(newIngredients: IngredientDto[]): void {

    const requests = newIngredients.map(ingredient => {
      return this.addIngredientAsPromise(ingredient);
    });

    Promise.all(requests).then(() => {
      this.loadIngredients().toPromise().then((ingredients: IngredientDto[]) => {
        this.ingredientsChanged.next(ingredients);
      });
    });
  }

  updateIngredient(ingredient: IngredientDto): void {

    this.shoppingListController.updateIngredient(ingredient).toPromise()
      .then(() => {
        this.loadIngredients().toPromise().then((ingredients: IngredientDto[]) => {
          this.ingredientsChanged.next(ingredients);
        });
      });
  }

  deleteIngredient(id: number): void {

    this.shoppingListController.deleteIngredientById(id).toPromise()
      .then(() => {
        this.loadIngredients().toPromise().then((ingredients: IngredientDto[]) => {
          this.ingredientsChanged.next(ingredients);
        });
      });
  }

  private addIngredientAsPromise(ingredient: IngredientDto): Promise<number> {

    return this.shoppingListController.addIngredient(ingredient).toPromise();
  }
}
