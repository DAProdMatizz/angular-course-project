import {Injectable} from '@angular/core';
import {AuthControllerService} from '../../controller/auth/auth-controller.service';
import {AuthenticationDto} from '../../model/auth/auth.model';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {UserDto} from '../../model/auth/user.model';
import {catchError, tap} from 'rxjs/operators';
import {LoggedUserDto} from '../../model/auth/logged-user.model';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly DEFAULT_ERROR_MESSAGE = 'Unknown error has occurred, please try again.';
  private readonly AUTHENTICATED_USER_KEY = 'authenticatedUser';

  loggedUser = new BehaviorSubject<LoggedUserDto>(null);

  constructor(private authController: AuthControllerService, private router: Router) {

  }

  private static userToLoggedUser(user: UserDto): LoggedUserDto {

    return new LoggedUserDto(user.token, user.username, user.identity);
  }

  authenticate(authData: AuthenticationDto): Observable<UserDto> {

    return this.authController.authenticate(authData)
      .pipe(tap(user => {
        this.handleUser(user);
      }), catchError(error => {
        return this.handleError(error);
      }));
  }

  autoLogin(): void {

    const user: UserDto = JSON.parse(localStorage.getItem(this.AUTHENTICATED_USER_KEY)) as UserDto;
    if (user) {
      const loggedUser = AuthService.userToLoggedUser(user);

      if (loggedUser.getToken()) {
        this.loggedUser.next(loggedUser);
      }
    }
  }

  logout(): void {

    this.loggedUser.next(null);
    this.router.navigate(['/auth']).then();
    localStorage.removeItem(this.AUTHENTICATED_USER_KEY);
  }

  private handleUser(user: UserDto): void {

    const authenticatedUser = AuthService.userToLoggedUser(user);
    this.loggedUser.next(authenticatedUser);
    localStorage.setItem(this.AUTHENTICATED_USER_KEY, JSON.stringify(authenticatedUser));
  }

  private handleError(error: any): Observable<UserDto> {

    if (!error.error) {
      return throwError(this.DEFAULT_ERROR_MESSAGE);
    }
    return throwError(error.error.message);
  }
}
