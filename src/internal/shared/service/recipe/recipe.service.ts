import {Injectable} from '@angular/core';
import {RecipeDto} from '../../model/recipe/recipe.model';
import {IngredientDto} from '../../model/ingredient/ingredient.model';
import {ShoppingListService} from '../shopping-list/shopping-list.service';
import {Observable, Subject} from 'rxjs';
import {RecipeControllerService} from '../../controller/recipe/recipe-controller.service';

@Injectable({providedIn: 'root'})
export class RecipeService {

  recipesChanged = new Subject<RecipeDto[]>();

  constructor(private shoppingListService: ShoppingListService, private recipeController: RecipeControllerService) {

  }

  loadAllRecipes(): Observable<RecipeDto[]> {

    return this.recipeController.loadAllRecipes();
  }

  addIngredientsToShoppingList(ingredients: IngredientDto[]): void {

    this.shoppingListService.addIngredients(ingredients);
  }

  getRecipeById(id: number): Observable<RecipeDto> {

    return this.recipeController.loadRecipeById(id);
  }

  addRecipe(recipe: RecipeDto): void {

    this.recipeController.addRecipe(recipe).toPromise()
      .then(() => {
        this.loadAllRecipes().toPromise().then((recipes: RecipeDto[]) => {
          this.recipesChanged.next(recipes);
        });
      });
  }

  updateRecipe(recipe: RecipeDto): void {

    this.recipeController.updateRecipe(recipe).toPromise()
      .then(() => {
        this.loadAllRecipes().toPromise().then((recipes: RecipeDto[]) => {
          this.recipesChanged.next(recipes);
        });
      });
  }

  deleteRecipe(id: number): void {

    this.recipeController.deleteRecipe(id).toPromise()
      .then(() => {
        this.loadAllRecipes().toPromise().then((recipes: RecipeDto[]) => {
          this.recipesChanged.next(recipes);
        });
      });
  }
}
