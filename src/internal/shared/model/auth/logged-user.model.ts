export class LoggedUserDto {

  constructor(private token: string, private username: string, private identity: string) {
  }

  getToken(): string {

    return this.token;
  }

  getUsername(): string {

    return this.username;
  }

  getIdentity(): string {

    return this.identity;
  }
}
