export interface UserDto {
  token: string;
  username: string;
  identity: string;
}
