export interface IngredientDto {

  id?: number;
  name: string;
  amount: number;
}
