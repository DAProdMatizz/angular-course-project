import {IngredientDto} from '../ingredient/ingredient.model';

export interface RecipeDto {

  id?: number;
  name: string;
  description: string;
  imagePath: string;
  ingredients: IngredientDto[];
}
