import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IngredientDto} from '../../model/ingredient/ingredient.model';
import {Observable} from 'rxjs';
import {Navigation} from '../../../../environments/navigation';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListControllerService {

  constructor(private httpClient: HttpClient) { }

  addIngredient(ingredient: IngredientDto): Observable<number> {

    return this.httpClient.post<number>(Navigation.addIngredient, ingredient);
  }

  loadShoppingListIngredients(): Observable<IngredientDto[]> {

    return this.httpClient.post<IngredientDto[]>(Navigation.loadAllIngredients, null);
  }

  loadIngredientById(id: number): Observable<IngredientDto> {

    return this.httpClient.post<IngredientDto>(Navigation.loadIngredientById(id), null);
  }

  updateIngredient(ingredient: IngredientDto): Observable<void> {

    return this.httpClient.post<void>(Navigation.updateIngredient, ingredient);
  }

  deleteIngredientById(id: number): Observable<void> {

    return this.httpClient.post<void>(Navigation.deleteIngredientById(id), null);
  }
}
