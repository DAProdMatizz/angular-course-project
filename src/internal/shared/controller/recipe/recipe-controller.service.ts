import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RecipeDto} from '../../model/recipe/recipe.model';
import {Observable} from 'rxjs';
import {Navigation} from '../../../../environments/navigation';

@Injectable({
  providedIn: 'root'
})
export class RecipeControllerService {

  constructor(private httpClient: HttpClient) {

  }

  addRecipe(recipe: RecipeDto): Observable<number> {

    return this.httpClient.post<number>(Navigation.addRecipe, recipe);
  }

  updateRecipe(recipe: RecipeDto): Observable<void> {

    return this.httpClient.post<void>(Navigation.updateRecipe, recipe);
  }

  loadAllRecipes(): Observable<RecipeDto[]> {

    return this.httpClient.post<RecipeDto[]>(Navigation.loadAllRecipes, null);
  }

  loadRecipeById(recipeId: number): Observable<RecipeDto> {

    return this.httpClient.post<RecipeDto>(Navigation.loadRecipeById(recipeId), null);
  }

  deleteRecipe(id: number): Observable<void> {

    return this.httpClient.post<void>(Navigation.deleteRecipeById(id), null);
  }
}
