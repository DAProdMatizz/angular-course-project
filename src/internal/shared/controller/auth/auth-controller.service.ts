import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthenticationDto} from '../../model/auth/auth.model';
import {Observable} from 'rxjs';
import {UserDto} from '../../model/auth/user.model';
import {Navigation} from '../../../../environments/navigation';

@Injectable({
  providedIn: 'root'
})
export class AuthControllerService {

  constructor(private httpClient: HttpClient) {
  }

  authenticate(authData: AuthenticationDto): Observable<UserDto> {

    return this.httpClient.post<UserDto>(Navigation.authenticate, authData);
  }
}
