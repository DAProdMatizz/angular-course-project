import {environment} from './environment';

export class Navigation {

  static readonly auth = '/authenticate';

  static readonly ingredients = '/ingredients';
  static readonly recipes = '/recipes';

  static readonly load = '/load';
  static readonly add = '/add';
  static readonly update = '/update';
  static readonly delete = '/delete';

  static readonly all = '/all';

  static readonly loadAll = Navigation.load + Navigation.all;

  static readonly authenticate = environment.baseUrl + Navigation.auth;

  static readonly loadAllIngredients = environment.baseUrl + Navigation.ingredients + Navigation.loadAll;
  static readonly addIngredient = environment.baseUrl + Navigation.ingredients + Navigation.add;
  static readonly updateIngredient = environment.baseUrl + Navigation.ingredients + Navigation.update;

  static readonly loadAllRecipes = environment.baseUrl + Navigation.recipes + Navigation.loadAll;
  static readonly addRecipe = environment.baseUrl + Navigation.recipes + Navigation.add;
  static readonly updateRecipe = environment.baseUrl + Navigation.recipes + Navigation.update;

  static loadRecipeById(id: number): string {

    return environment.baseUrl + Navigation.recipes + Navigation.loadById(id);
  }

  static deleteRecipeById(id: number): string {

    return environment.baseUrl + Navigation.recipes + Navigation.deleteById(id);
  }

  static loadIngredientById(id: number): string {

    return environment.baseUrl + Navigation.ingredients + Navigation.loadById(id);
  }

  static deleteIngredientById(id: number): string {

    return environment.baseUrl + Navigation.ingredients + Navigation.deleteById(id);
  }

  private static loadById(id: number): string {

    return Navigation.load + Navigation.idPathVariable(id);
  }

  private static deleteById(id: number): string {

    return Navigation.delete + Navigation.idPathVariable(id);
  }

  private static idPathVariable(id: number): string {

    return '/' + String(id);
  }
}
