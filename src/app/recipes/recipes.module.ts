import {NgModule} from '@angular/core';
import {RecipesComponent} from './recipes.component';
import {RecipeDetailComponent} from './recipe-detail/recipe-detail.component';
import {RecipeStartComponent} from './recipe-start/recipe-start.component';
import {RecipeEditComponent} from './recipe-edit/recipe-edit.component';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {RecipesRoutingModule} from './recipes-routing.module';
import {CoreModule} from '../../internal/core/core.module';
import {RecipeListModule} from './recipe-list/recipe-list.module';

@NgModule({
  declarations: [
    RecipesComponent,
    RecipeDetailComponent,
    RecipeStartComponent,
    RecipeEditComponent
  ],
  imports: [
    CoreModule,
    RouterModule,
    ReactiveFormsModule,
    RecipesRoutingModule,
    RecipeListModule
  ]
})
export class RecipesModule {
}
