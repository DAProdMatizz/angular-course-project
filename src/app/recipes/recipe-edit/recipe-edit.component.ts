import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {RecipeService} from '../../../internal/shared/service/recipe/recipe.service';
import {RecipeDto} from '../../../internal/shared/model/recipe/recipe.model';
import {IngredientDto} from '../../../internal/shared/model/ingredient/ingredient.model';
import {CustomValidator} from '../../../internal/core/validator/custom.validator';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  private readonly ID_STRING = 'id';
  private readonly INGREDIENTS_STRING = 'ingredients';

  id: number;
  editMode: boolean;
  recipeForm: FormGroup;

  private static createIngredientsFormGroup(name?: string, amount?: number): FormGroup {

    if (name !== undefined && amount !== undefined) {
      return new FormGroup({
        name: new FormControl(name, Validators.required),
        amount: new FormControl(amount, [Validators.required, CustomValidator.invalidNumber])
      });
    }
    return new FormGroup({
      name: new FormControl(null, Validators.required),
      amount: new FormControl(null, [Validators.required, CustomValidator.invalidNumber])
    });
  }

  constructor(private router: Router, private route: ActivatedRoute, private recipeService: RecipeService) {

    this.editMode = false;
  }

  ngOnInit(): void {

    this.recipeForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      imagePath: new FormControl(null, Validators.required),
      description: new FormControl(null),
      ingredients: new FormArray([])
    });

    this.route.params.subscribe((params: Params) => {
      this.id = Number(params[this.ID_STRING]);
      this.editMode = params[this.ID_STRING] != null;

      this.initForm();
    });
  }

  onSubmit(): void {

    if (this.editMode) {
      this.recipeService.updateRecipe({id: this.id, ...this.recipeForm.value});
    } else {
      this.recipeService.addRecipe(this.recipeForm.value);
    }

    this.onCancel();
  }

  onAddIngredient(): void {

    (this.recipeForm.get(this.INGREDIENTS_STRING) as FormArray).push(RecipeEditComponent.createIngredientsFormGroup());
  }

  onCancel(): void {

    this.router.navigate(['../'], {relativeTo: this.route}).then();
  }

  onDeleteIngredient(index: number): void {

    this.recipeIngredientsAsFormArray().removeAt(index);
  }

  getIngredients(): AbstractControl[] {

    return this.recipeIngredientsAsFormArray().controls;
  }

  private initForm(): void {

    this.getRecipeForForm().then((recipe: RecipeDto) => {
      const recipeName = recipe.name;
      const recipeImagePath = recipe.imagePath;
      const recipeDescription = recipe.description;

      this.recipeForm.patchValue({name: recipeName, imagePath: recipeImagePath, description: recipeDescription});

      recipe.ingredients.forEach((ingredient: IngredientDto) => {
        this.recipeIngredientsAsFormArray().push(RecipeEditComponent.createIngredientsFormGroup(ingredient.name, ingredient.amount));
      });
    });
  }

  private getRecipeForForm(): Promise<RecipeDto> {

    if (this.editMode && this.id !== undefined) {
      return this.recipeService.getRecipeById(this.id).toPromise();
    }

    return new Promise<RecipeDto>(resolve => {
      resolve({name: '', description: '', imagePath: '', ingredients: []});
    });
  }

  private recipeIngredientsAsFormArray(): FormArray {

    return (this.recipeForm.get(this.INGREDIENTS_STRING) as FormArray);
  }
}
