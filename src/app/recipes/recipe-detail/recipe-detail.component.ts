import {Component, OnInit} from '@angular/core';
import {RecipeDto} from '../../../internal/shared/model/recipe/recipe.model';
import {RecipeService} from '../../../internal/shared/service/recipe/recipe.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  private readonly ID_NAME = 'id';

  recipeId: number;
  recipe: RecipeDto = {name: '', description: '', imagePath: '', ingredients: []};

  constructor(private recipeService: RecipeService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      this.recipeId = Number(params[this.ID_NAME]);

      this.recipeService.getRecipeById(this.recipeId).subscribe((recipe: RecipeDto) => {
        if (recipe) {
          this.recipe = recipe;
        } else {
          this.router.navigate(['../'], {relativeTo: this.route}).then();
        }
      });
    });
  }

  onAddToShoppingList(): void {

    this.recipeService.addIngredientsToShoppingList(this.recipe.ingredients);
  }

  onEditRecipe(): void {

    this.router.navigate(['edit'], {relativeTo: this.route}).then();
  }

  onDeleteRecipe(): void {

    this.recipeService.deleteRecipe(this.recipeId);
    this.router.navigate(['../'], {relativeTo: this.route}).then();
  }
}
