import {Component, OnInit} from '@angular/core';
import {RecipeDto} from '../../../internal/shared/model/recipe/recipe.model';
import {RecipeService} from '../../../internal/shared/service/recipe/recipe.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  private recipesChangedSubscription: Subscription;
  recipes: RecipeDto[];

  constructor(private recipeService: RecipeService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit(): void {

    this.recipeService.loadAllRecipes().subscribe(recipes => {
      this.recipes = recipes;
    });

    this.recipesChangedSubscription = this.recipeService.recipesChanged.subscribe((recipes: RecipeDto[]) => {
      this.recipes = recipes;
    });
  }

  onNewRecipe(): void {

    this.router.navigate(['new'], {relativeTo: this.route}).then();
  }
}
