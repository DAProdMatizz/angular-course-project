import {NgModule} from '@angular/core';
import {RecipeListComponent} from './recipe-list.component';
import {RecipeItemComponent} from './recipe-item/recipe-item.component';
import {CoreModule} from '../../../internal/core/core.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    RecipeListComponent,
    RecipeItemComponent
  ],
  imports: [
    CoreModule,
    RouterModule
  ],
  exports: [
    RecipeListComponent
  ]
})
export class RecipeListModule {
}
