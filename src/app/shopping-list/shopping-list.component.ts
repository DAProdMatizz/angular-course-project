import {Component, OnDestroy, OnInit} from '@angular/core';
import {IngredientDto} from '../../internal/shared/model/ingredient/ingredient.model';
import {ShoppingListService} from '../../internal/shared/service/shopping-list/shopping-list.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {

  private ingredientChangeSubscription: Subscription;

  ingredients: IngredientDto[];

  constructor(private shoppingListService: ShoppingListService) {
  }

  ngOnInit(): void {

    this.shoppingListService.loadIngredients().subscribe(ingredients => {
      this.ingredients = ingredients;
    });

    this.ingredientChangeSubscription = this.shoppingListService.ingredientsChanged.subscribe((ingredients: IngredientDto[]) => {
      this.ingredients = ingredients;
    });
  }

  ngOnDestroy(): void {

    this.ingredientChangeSubscription.unsubscribe();
  }

  onEditIngredient(id: number): void {

    this.shoppingListService.startedEditing.next(id);
  }
}
