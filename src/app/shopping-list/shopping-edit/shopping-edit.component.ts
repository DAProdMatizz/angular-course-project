import {Component, OnDestroy, OnInit} from '@angular/core';
import {ShoppingListService} from '../../../internal/shared/service/shopping-list/shopping-list.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {CustomValidator} from '../../../internal/core/validator/custom.validator';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  startedEditingSubscription: Subscription;
  editMode: boolean;
  editedIngredientId: number;
  shoppingEditForm: FormGroup;

  constructor(private shoppingListService: ShoppingListService) {

    this.editMode = false;
  }

  ngOnInit(): void {

    this.initForm();

    this.startedEditingSubscription = this.shoppingListService.startedEditing.subscribe((index: number) => {
      this.editedIngredientId = index;
      this.editMode = true;
      this.shoppingListService.getIngredientById(index).subscribe(ingredient => {
        this.shoppingEditForm.setValue({
          name: ingredient.name,
          amount: ingredient.amount
        });
      });
    });
  }

  ngOnDestroy(): void {

    this.startedEditingSubscription.unsubscribe();
  }

  onSaveIngredient(): void {

    const value = this.shoppingEditForm.value;

    if (this.editMode) {
      this.shoppingListService.updateIngredient({id: this.editedIngredientId, name: value.name, amount: value.amount});
    } else {
      this.shoppingListService.addIngredient(value);
    }

    this.onClear();
    this.ngOnInit();
  }

  onDelete(): void {

    this.shoppingListService.deleteIngredient(this.editedIngredientId);
    this.onClear();
  }

  onClear(): void {

    this.editMode = false;
    this.shoppingEditForm.reset();
  }

  getAddOrEditButtonName(): string {

    return this.editMode ? 'Update item' : 'Add item';
  }

  private initForm(): void {

    this.shoppingEditForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      amount: new FormControl(null, [Validators.required, CustomValidator.invalidNumber])
    });
  }
}
