import {NgModule} from '@angular/core';
import {ShoppingListComponent} from './shopping-list.component';
import {ShoppingEditComponent} from './shopping-edit/shopping-edit.component';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthGuardService} from '../../internal/core/auth/auth-guard.service';
import {CoreModule} from '../../internal/core/core.module';

@NgModule({
  declarations: [
    ShoppingListComponent,
    ShoppingEditComponent
  ],
  imports: [
    CoreModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {path: '', component: ShoppingListComponent, canActivate: [AuthGuardService]}
    ])
  ]
})
export class ShoppingListModule {
}
