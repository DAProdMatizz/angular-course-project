import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../internal/shared/service/authenticate/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  private loggedUserSubscription: Subscription;

  isAuthenticated: boolean;

  constructor(private authService: AuthService) {

    this.isAuthenticated = false;
  }

  ngOnInit(): void {

    this.loggedUserSubscription = this.authService.loggedUser.subscribe(user => {
      this.isAuthenticated = !!user;
    });
  }

  ngOnDestroy(): void {

    this.loggedUserSubscription.unsubscribe();
  }

  onLogout(): void {

    this.authService.logout();
  }
}
