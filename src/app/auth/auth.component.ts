import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../internal/shared/service/authenticate/auth.service';
import {Router} from '@angular/router';
import {Navigation} from '../../environments/navigation';
import {AuthAlertComponent} from '../../internal/core/auth/auth-alert/auth-alert.component';
import {PlaceholderDirective} from '../../internal/core/placeholder/placeholder.directive';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {

  @ViewChild(PlaceholderDirective, {static: false}) authAlertHost: PlaceholderDirective;
  authForm: FormGroup;
  isLoading: boolean;

  private closeAuthAlertSubscription: Subscription;

  constructor(private authService: AuthService, private router: Router, private componentFactoryResolver: ComponentFactoryResolver) {

    this.isLoading = false;
  }

  ngOnInit(): void {

    this.authForm = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  ngOnDestroy(): void {

    if (this.closeAuthAlertSubscription) {
      this.closeAuthAlertSubscription.unsubscribe();
    }
  }


  onSubmit(): void {

    this.isLoading = true;

    this.authService.authenticate(this.authForm.value).subscribe(() => {
      this.isLoading = false;
      this.router.navigate([Navigation.recipes]).then();
    }, (errorMessage: string) => {
      this.showAuthAlert(errorMessage);
      this.isLoading = false;
    });

    this.authForm.reset();
  }

  private showAuthAlert(errorMessage: string): void {

    const authAlertComponentFactory = this.componentFactoryResolver.resolveComponentFactory(AuthAlertComponent);
    const hostViewContainerRef = this.authAlertHost.viewContainerRef;
    hostViewContainerRef.clear();

    const componentRef = hostViewContainerRef.createComponent(authAlertComponentFactory);
    componentRef.instance.errorMessage = errorMessage;
    this.closeAuthAlertSubscription = componentRef.instance.close.subscribe(() => {
      this.closeAuthAlertSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
  }
}
